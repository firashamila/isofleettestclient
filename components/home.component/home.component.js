const homeController = function ($scope, restaurantService) {
  $scope.items             = [];
  $scope.selected          = {};
  $scope.restaurants       = [];
  $scope.itemsLoaded       = false;
  $scope.restaurantsLoaded = false;

  restaurantService.getRestaurants().then(
    function (value) {
      $scope.restaurants = value.data;
      $scope.restaurantsLoaded = true;
      $scope.itemsLoaded = true;
    }
  );

  $scope.checked = function () {
    $scope.itemsLoaded        = false;
    const targetedRestaurants = _.filter(_.keys($scope.selected), function (key) {
      return $scope.selected[key]
    });
    $scope.items = [];
    _.forEach(targetedRestaurants, function (id, index) {
      restaurantService.getRestaurantItem(id).then(
        function (value) {
          $scope.itemsLoaded = true;
          $scope.items = _.concat($scope.items, value.data);
        }
      ).catch(console.error);
    });
  }
};
