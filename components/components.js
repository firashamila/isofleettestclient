app.component(
  'home',
  {
    templateUrl: './components/home.component/home.component.html',
    controller: homeController
  }
);
app.component(
  'admin',
  {
    templateUrl: './components/admin.component/admin.component.html',
    controller: adminController
  }
);
app.component(
  'signUp',
  {
    templateUrl: './components/signup.component/signup.component.html',
    controller: signUpController
  }
);
app.component(
  'restaurant',
  {
    templateUrl: './components/restaurant.component/restaurant.component.html',
    controller: restaurantController
  }
);
app.component(
  'login',
  {
    templateUrl: './components/login.component/login.component.html',
    controller: loginController
  }
);
