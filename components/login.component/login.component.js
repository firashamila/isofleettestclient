const loginController = function ($scope, restaurantService, $state) {
  $scope.restaurant = {}
  $scope.login = function () {
    $scope.error = '';
    restaurantService.login($scope.restaurant).then(
      function (value) {
        $state.go('restaurant', {id: value.data.id});
      }
    ).catch(
      function (reason) {
        $scope.error = 'bad credentials'
      }
    )
  }
};
