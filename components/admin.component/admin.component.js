const adminController = function ($scope, restaurantService) {
  $scope.restaurants = [];

  restaurantService.getRestaurants().then(
    function (res) {
      $scope.restaurants = res.data;
    }
  ).catch(console.error);

  $scope.accept = function (restaurant) {
    restaurantService.acceptRestaurant(restaurant).then(
      function (value) {
        restaurant.is_active = true;
      }
    ).catch(console.error);
  };

  $scope.refuse = function (restaurantId) {
    restaurantService.deleteRestaurant(restaurantId).then(
      function (value) {
        _.remove($scope.restaurants, ['id', restaurantId]);
      }
    ).catch(console.error);
  }
};
