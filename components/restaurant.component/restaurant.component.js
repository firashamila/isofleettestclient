const restaurantController = function ($scope, restaurantService, $stateParams) {
  $scope.restaurant  = {};
  $scope.items       = [];
  $scope.showAddForm = false;
  $scope.item        = {
    restaurantId: $stateParams.id
  };
  restaurantService.getRestaurantById($stateParams.id).then(
    function (value) {
      $scope.restaurant = value.data;
    }
  ).catch(console.error);
  restaurantService.getRestaurantItem($stateParams.id).then(
    function (value) {
      $scope.items = value.data;
    }
  ).catch(console.error);
  $scope.submitItem = function () {
    restaurantService.addItem($scope.item).then(
      function (value) {
        $scope.item        = {
          restaurantId: $stateParams.id
        };
        $scope.showAddForm = false;
        $scope.items.unshift(value.data.data)
      }
    ).catch(console.error);
  }
  $scope.deleteItem = function(itemId) {
    restaurantService.deleteItem(itemId).then(
      function (value) {
        _.remove($scope.items, ['id', itemId]);
      }
    ).catch(console.error);
  }
};
