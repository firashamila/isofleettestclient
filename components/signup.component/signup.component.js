const signUpController = function ($scope, authenticationService, $http, $state) {
  $scope.restaurant = {
    name: '',
    description: '',
    openingFrom: '',
    openingTo: '',
    address: '',
    email: '',
    username: '',
    password: '',
    website: ''
  };
  $scope.confirmed = -1;
  $scope.confirmationKeyUp = function (event) {
    $scope.confirmed = +(event ===$scope.restaurant.password);
  };
  $scope.submitSignup = function () {
    if ($scope.confirmed === 1) {
      $http.post(APIS.restaurant, $scope.restaurant).then(
        function (value) {
          $state.go('restaurant', {id: value.data.data.id})
        }
      ).catch(console.error);
    }
  }
};
