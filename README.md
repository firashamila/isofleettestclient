
## In use Libs
    - angular 1.7.5
    - @uirouter/angularjs
    - angular-material
    - lodash
--
## installation
    $ npm install
## Starting the live client server 
    $ npm run serve
---
 
## Routes
* `/`: home page for browsing Items by restaurants
* `/admin`: delete and approve restaurants
* `/signUp`: create new restaurant 
* `/restaurant/{id}` : restaurant administration, add/delete items
* `/login`: login for restaurants 