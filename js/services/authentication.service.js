app.service('authenticationService', function ($http) {
  const signUp = function(signupForm) {
    $http.post(APIS.restaurant, signupForm);
  };
  return {
    signUp: signUp
  }
});
