app.service('restaurantService', function ($http) {
  const getRestaurants = function () {
    return $http.get(APIS.restaurant);
  };

  const deleteRestaurant = function (restaurantId) {
    return $http.delete(APIS.restaurant + '?id='+ restaurantId);
  };

  const deleteItem = function (itemId) {
    return $http.delete(APIS.item + '?id='+ itemId);
  };

  const acceptRestaurant = function (restaurantId) {
    return $http.put(APIS.restaurant, {id: restaurantId});
  };

  const getRestaurantById = function (id) {
    return $http.get(APIS.restaurant + '/' + id);
  };

  const addItem = function(item) {
    return $http.post(APIS.item, item);
  };

  const getRestaurantItem = function (id) {
    return $http.get(APIS.item + '/' + id)
  };

  const login = function (data) {
    return $http.post(APIS.auth, data);
  };

  return {
    login:             login,
    addItem:           addItem,
    deleteItem:        deleteItem,
    getRestaurants:    getRestaurants,
    deleteRestaurant:  deleteRestaurant,
    acceptRestaurant:  acceptRestaurant,
    getRestaurantById: getRestaurantById,
    getRestaurantItem: getRestaurantItem
  }
});
