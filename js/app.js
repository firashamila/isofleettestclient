const serverAddress = 'http://localhost:8000/api';
const APIS = {
  item : serverAddress + '/item',
  auth: serverAddress + '/restaurant/auth',
  restaurant: serverAddress + '/restaurant'
};
const app = angular.module('ng-sm', ['ngMaterial', 'ngMessages', 'ui.router']);
app.config(function ($stateProvider, $urlRouterProvider) {
  STATES.forEach(function (state) {
    $stateProvider.state(state);
  });
  $urlRouterProvider.otherwise('/');
});
