const STATES = [
  {
    name: 'home',
    url: '/',
    component: 'home'
  },
  {
    name: 'admin',
    url: '/admin',
    component: 'admin'
  },
  {
    name: 'signUp',
    url: '/signUp',
    component: 'signUp'
  },
  {
    name: 'restaurant',
    url: '/restaurant/:id',
    component: 'restaurant'
  },
  {
    name: 'login',
    url: '/login',
    component: 'login'
  }
];
